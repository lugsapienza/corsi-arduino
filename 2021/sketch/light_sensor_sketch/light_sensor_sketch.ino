int light = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  light = analogRead(A0);
  Serial.println(light);
  if(light > 450) { 
    Serial.println("It is quite light!");
  }
  else if(light > 229 && light < 451) { 
    Serial.println("It is average light!");
  }
  else {
    Serial.println("It is pretty dark!");
  }
  delay(1000);
}
