#include <DHT.h>
#include <LiquidCrystal.h>

#define DHT_TYPE            DHT22
#define DHT_PIN             8   
#define WATER_RELAY_PIN     13 
#define LIGHT_RELAY_PIN     10
#define BRIGHT_PIN          A0
#define MOISTURE_PIN        A1 

int         luminosita_ambiente;
float       umidita_terreno;
float       umidita_aria;
float       temperatura_aria;

DHT dht(DHT_PIN, DHT_TYPE);
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

byte termometer[8] = {
0b00000,
0b10101,
0b01110,
0b11111,
0b01110,
0b10101,
0b00000,
0b00000
};

byte water[8] = {
0b00000,
0b00100,
0b01110,
0b11111,
0b11111,
0b01110,
0b00000,
0b00000
};

byte celsius[8] = {
0b00111,
0b00101,
0b00111,
0b00000,
0b00000,
0b00000,
0b00000,
0b00000
};

void setup()
{
  Serial.begin(9600);
  lcd.createChar(0, termometer);
  lcd.createChar(1, water);
  lcd.createChar(2, celsius);
  lcd.begin(16, 2);
  lcd.clear();
  pinMode(WATER_RELAY_PIN, OUTPUT);
  pinMode(LIGHT_RELAY_PIN, OUTPUT);
  dht.begin();
}

void loop()
{
  delay(2000);  
  umidita_aria = dht.readHumidity();
  Serial.print("Umidità Aria: ");
  Serial.println(umidita_aria);
  temperatura_aria= dht.readTemperature();
  Serial.print("Temperatura: ");
  Serial.println(temperatura_aria);
  luminosita_ambiente = analogRead(BRIGHT_PIN);
  for (int i = 0; i <= 100; i++) {
    umidita_terreno += analogRead(MOISTURE_PIN);
    delay(1);
  } 
  umidita_terreno = umidita_terreno/100.0;
  Serial.print("Umidità terreno: ");
  Serial.println(umidita_terreno);
  if (umidita_terreno < 30)
    digitalWrite(WATER_RELAY_PIN, HIGH);
  if (umidita_terreno > 60)
    digitalWrite(WATER_RELAY_PIN, LOW);
  if (luminosita_ambiente < 100)
    digitalWrite(LIGHT_RELAY_PIN, HIGH);
  if (luminosita_ambiente > 100)
    digitalWrite(LIGHT_RELAY_PIN, LOW);
  lcd.setCursor(0, 0);
  lcd.write(byte(1));
  lcd.print(" Umid: ");
  lcd.print(umidita_aria);
  lcd.print("  %");
  lcd.setCursor(0, 1);
  lcd.write(byte(0));
  lcd.print(" Temp: ");
  lcd.print(temperatura_aria);
  lcd.print(" ");
  lcd.write(byte(2));
  lcd.print("C");
}
