int pinOut = 10;

void setup() {
  Serial.begin(9600);
  pinMode(10, OUTPUT);
}

void loop() {
  digitalWrite(pinOut, LOW);
  Serial.println("The relay is off");
  delay(500);
  digitalWrite(pinOut, HIGH);
  Serial.println("The relay is on");
  delay(500);
}
