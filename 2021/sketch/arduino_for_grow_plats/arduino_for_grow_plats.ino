#include <DHT.h>
#include <LiquidCrystal.h>
#include <SoftwareSerial.h>

#define DHT_TYPE            DHT22
#define DHT_PIN             8   
#define WATER_RELAY_PIN     13 
#define LIGHT_RELAY_PIN     10
#define BRIGHT_PIN          A0
#define MOISTURE_PIN        A1 

int         lenHtml;
int         luminosita_ambiente;
float       umidita_terreno;
float       umidita_aria;
float       temperatura_aria;
char        reply[500];
char        command[50];
char        html[50];
char        temp[10];

DHT dht(DHT_PIN, DHT_TYPE);
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
SoftwareSerial ESPserial(6, 7); // RX | TX

byte termometer[8] = {
0b00000,
0b10101,
0b01110,
0b11111,
0b01110,
0b10101,
0b00000,
0b00000
};

byte water[8] = {
0b00000,
0b00100,
0b01110,
0b11111,
0b11111,
0b01110,
0b00000,
0b00000
};

byte celsius[8] = {
0b00111,
0b00101,
0b00111,
0b00000,
0b00000,
0b00000,
0b00000,
0b00000
};

void setup()
{
  Serial.begin(9600);
  ESPserial.begin(9600);
  ESPserial.write("AT+RST\r\n");
  getReply(2000);
  ESPserial.write("AT+CWMODE=1\r\n");
  getReply(1500);
  //ESPserial.print("AT+CWJAP=\"Infostrada-0D527C\",\"XL8DGA3QB4\"\r\n");
  //getReply(6000);
  ESPserial.print("AT+CIFSR\r\n");
  getReply(1000);
  ESPserial.write("AT+CIPMUX=1\r\n");
  getReply(1500);
  ESPserial.write("AT+CIPSERVER=1,80\r\n");
  getReply(1500); 
  lcd.createChar(0, termometer);
  lcd.createChar(1, water);
  lcd.createChar(2, celsius);
  pinMode(WATER_RELAY_PIN, OUTPUT);
  pinMode(LIGHT_RELAY_PIN, OUTPUT);
  dht.begin();
  lcd.begin(16, 2);
  lcd.clear();

}

void loop()
{
  delay(2000);  
  umidita_aria = dht.readHumidity();
  temperatura_aria= dht.readTemperature();
  luminosita_ambiente = analogRead(BRIGHT_PIN);
  for (int i = 0; i <= 100; i++) {
    umidita_terreno += analogRead(MOISTURE_PIN);
    delay(1);
  } 
  umidita_terreno = umidita_terreno/100.0;
  if (umidita_terreno < 30)
    digitalWrite(WATER_RELAY_PIN, HIGH);
  if (umidita_terreno > 60)
    digitalWrite(WATER_RELAY_PIN, LOW);
  if (luminosita_ambiente < 30)
    digitalWrite(WATER_RELAY_PIN, HIGH);
  if (luminosita_ambiente > 30)
    digitalWrite(WATER_RELAY_PIN, LOW);
  lcd.setCursor(0, 0);
  lcd.write(byte(1));
  lcd.print(" Umid: ");
  lcd.print(umidita_aria);
  lcd.print("  %");
  lcd.setCursor(0, 1);
  lcd.write(byte(0));
  lcd.print(" Temp: ");
  lcd.print(temperatura_aria);
  lcd.print(" ");
  lcd.write(byte(2));
  lcd.print("C");
  if(ESPserial.available()) {
    getReply( 2000 );
    bool foundIPD = false;
    for (int i=0; i<strlen(reply); i++)
      if ((reply[i]=='I') && (reply[i+1]=='P') && (reply[i+2]=='D'))
        foundIPD = true;
    if (foundIPD) {
      strcpy(html,"<html><head></head><body>");
      strcpy(command,"AT+CIPSEND=0,25\r\n");
      ESPserial.print(command);
      getReply(2000);          
      ESPserial.print(html);
      getReply(2000);   
      
      strcpy(html,"<h1>Temperatura: ");                
      dtostrf(temperatura_aria, 3, 2, temp);
      strcat(html, temp);               
      strcat(html, " &#176;C");
      lenHtml = strlen(html);
      strcpy(command,"AT+CIPSEND=0,");
      itoa(lenHtml, temp, 10);
      strcat(command, temp);
      strcat(command, "\r\n");
      ESPserial.print(command);
      getReply(2000); 
      ESPserial.print(html);
      getReply(2000);

      strcpy(html,"<h1>Umidit&#224; terreno: ");                
      dtostrf(umidita_terreno, 3, 2, temp);
      strcat(html, temp);               
      strcat(html, "%</h1>");
      lenHtml = strlen(html);
      strcpy(command,"AT+CIPSEND=0,");
      itoa(lenHtml, temp, 10);
      strcat(command, temp);
      strcat(command, "\r\n");
      ESPserial.print(command);
      getReply(2000); 
      ESPserial.print(html);
      getReply(2000);

      strcpy(html,"<h1>Umidit&#224; aria: "); 
      dtostrf(umidita_aria, 3, 2, temp);               
      strcat(html, temp);               
      strcat(html, "%</h1>");
      lenHtml = strlen(html);
      strcpy(command,"AT+CIPSEND=0,");
      itoa(lenHtml, temp, 10);
      strcat(command, temp);
      strcat(command, "\r\n");
      ESPserial.print(command);
      getReply(2000); 
      ESPserial.print(html);
      getReply(2000);

      strcpy(html,"<h1>Luminosit&#224;: "); 
      dtostrf(luminosita_ambiente, 3, 2, temp);               
      strcat(html, temp);               
      strcat(html, "</h1>");
      lenHtml = strlen(html);
      strcpy(command,"AT+CIPSEND=0,");
      itoa(lenHtml, temp, 10);
      strcat(command, temp);
      strcat(command, "\r\n");
      ESPserial.print(command);
      getReply(2000); 
      ESPserial.print(html);
      getReply(2000);

      strcpy(html,"</body></html>");
      strcpy(command,"AT+CIPSEND=0,14\r\n");
      ESPserial.print(command);
      getReply(2000); 
      ESPserial.print(html);
      getReply(2000); 

      // close the connection
      ESPserial.print( "AT+CIPCLOSE=0\r\n" );
      getReply(2000);            

    }
  }           
}

void getReply(int wait) {
    int tempPos = 0;
    long int time = millis();
    while( (time + wait) > millis()) {
        while(ESPserial.available() && tempPos < 500) {
            reply[tempPos] = ESPserial.read();
            tempPos++;
        }
        reply[tempPos] = 0;
    } 
    Serial.println(reply);
    Serial.println("-----\n\r");
}
